import React from 'react';
export default class HomePage extends React.Component {

    render(){
        return(
            <div style={{flex: 1, flexDirection:'row',justifyContent:'center'}}>
                <h1 style={{color: 'red', textAlign: 'center'}}>Home</h1>
            </div>
        );
    }
}