import React from 'react';
import {
  BrowserRouter,
} from 'react-router-dom';
import routesProps from './constants/routesProps';
import AuthRoutes from './components/AuthRoutes';


const App: React.FC = () => {
  return (
    <BrowserRouter >
      <AuthRoutes
        routesProps={routesProps}
      />
    </BrowserRouter>
  );
}

export default App;
