import { RouteProps } from 'react-router-dom';
import TasksPage from '../pages/tasksPage';
import ProfilePage from '../pages/profilePage';
import HomePage from '../pages/homePage';
import LoginPage from '../pages/loginPage';

const routesProps: RouteProps[] =
    [
        {
            path: "/",
            component: HomePage,
            sensitive: false,
        },

        {
            path: "/login",
            component: LoginPage,
            sensitive: false,
        },

        {
            path: "/profile",
            component: ProfilePage,
            sensitive: true,
        },
        {
            path: "/tasks",
            component: TasksPage,
            sensitive: true,
        },

    ];
export default routesProps;