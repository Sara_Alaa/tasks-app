import React from 'react';
import {
    Route,
    RouteProps,
    Switch,
    Redirect,
} from 'react-router-dom';
import LoginPage from '../../pages/loginPage';
import HomePage from '../../pages/homePage';

interface IProps {
    routesProps: RouteProps[],

}
interface IStates {
    isLoggedIn: boolean,
}

export default class AuthRoutes extends React.Component<IProps, IStates> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            isLoggedIn: true,
        }
    }
    render() {

        const { routesProps } = this.props;
        return (

            <Switch>
                {

                    routesProps.map((routeProps, index) => {
                        console.log('index', index);
                        if (!this.state.isLoggedIn && routeProps.sensitive) {
                            return (
                                <Redirect  to= "/login" />
                            );
                        } else {
                            return (
                                <Route
                                    exact
                                    path={routeProps.path}
                                    component={routeProps.component}

                                ></Route>

                            );
                        }
                    }

                )
            }
            </Switch>


        );
    }
}

